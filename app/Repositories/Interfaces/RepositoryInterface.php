<?php

namespace TodoList\Repositories\Interfaces;


interface RepositoryInterface
{
    public function load($id);
}