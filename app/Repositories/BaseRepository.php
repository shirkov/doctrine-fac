<?php

namespace TodoList\Repositories;

use Doctrine\ORM\EntityRepository;
use TodoList\Repositories\Interfaces\RepositoryInterface;

abstract class BaseRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param int $id
     *
     * @return object|null
     */
    public function load($id)
    {
        $entity = $this->findOneBy(['id' => $id]);

        return $entity;
    }
}