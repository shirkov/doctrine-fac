<?php

namespace TodoList\Http\Controllers;

use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Http\Request;
use TodoList\Entities\Task;
use TodoList\Repositories\TaskRepository;

class TaskController extends Controller
{

    /**
     * @var TaskRepository $taskRepository
     */
    private $taskRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->taskRepository = $em->getRepository(Task::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(EntityManagerInterface $em)
    {
        $tasks = $this->taskRepository->findAll();

        return view('pages.index')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('pages.add');
    }

    /**
     * Store current task in DB
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function store(Request $request, EntityManagerInterface $em)
    {
        $task = new Task(
            $request->get('name'),
            $request->get('description')
        );

        //@TODO: implement this method
//        $this->taskRepository->create();

        $em->persist($task);
        $em->flush();

        return redirect('create')->with('success_message', 'Task "' . $task->getName() . '" was added successfully!');
    }

    /**
     * Toggle task status from done to not done and vice versa
     *
     * @param $id
     */
    public function toggle($id, EntityManagerInterface $em)
    {
        $task = $this->taskRepository->load($id);

        /* @var Task $task */
        $task->toggleStatus();
        $currentStatus = $task->isDone() ? 'Done' : 'Not done';

        $em->flush();

        return redirect('tasks')->with('success_message', 'Task successfully marked as "' . $currentStatus . '"');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, EntityManagerInterface $em)
    {
        $task = $em->find(Task::class, $id);

        return view('pages.edit')->with(['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, EntityManagerInterface $em)
    {
        /* @var Task $task */
        $task = $em->find(Task::class, $id);

        $task->setName($request->get('name'))
            ->setDescription($request->get('description'));

        $em->flush();

        return redirect()->route('tasks.edit', ['id' => $id])->with('success_message', 'Task was updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, EntityManagerInterface $em)
    {
        $task = $em->find(Task::class, $id);

        $em->remove($task);
        $em->flush();

        return redirect('tasks')->with('success_message', 'Task successfully removed');
    }
}
