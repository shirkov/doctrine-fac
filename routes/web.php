<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use TodoList\Entities\Category;
use TodoList\Entities\Task;

Route::get('/test-category', function (\Doctrine\ORM\EntityManagerInterface $em){
//    $cat = new Category();
//    $cat->setImportance('!!!');
//
//    $em->persist($cat);
//    $em->flush();

    /**
     * @var $cat Category
     */
    /**
     * @var $task Task
     */
//    $task = $em->find(Task::class, '15');

//    $task->setCategory($cat);
//    $cat->setTask($task);

//    $em->flush();

//    $cat->setTasks($task);
//    $em->flush();

});

Route::get('/tasks', 'TaskController@index')->name('tasks.index');

Route::get('/create', 'TaskController@create')->name('tasks.create');

Route::post('/add', 'TaskController@store')->name('tasks.store');

Route::put('/toggle/{id}', 'TaskController@toggle')
    ->where('id', '[0-9]+')
    ->name('tasks.toggle');

Route::get('/tasks/{id}/edit', 'TaskController@edit')
    ->where('id', '[0-9]+')
    ->name('tasks.edit');

Route::put('/tasks/{id}', 'TaskController@update')
    ->where('id', '[0-9]+')
    ->name('tasks.update');

Route::delete('tasks/{id}', 'TaskController@destroy')
    ->where('id', '[0-9]+')
    ->name('tasks.destroy');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
