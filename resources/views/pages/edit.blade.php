@php /** @var TodoList\Entities\Task $task; **/ @endphp
@extends('layouts.master')
@section('title') Add a Task @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('tasks.index') }}" class="btn btn-sm btn-primary d-inline-block mb-3">
                <i class="fa fa-arrow-left"></i>
            </a>
            <h3>Edit Task</h3>
            <p>Use the following form to <u>edit</u> a new task to the system.</p>

            <hr>

            <form action="{{ route('tasks.update', ['id' => $task->getId()]) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

                <p><input autofocus type="text" value="{{$task->getName()}}" placeholder="Name..." name="name" class="form-control" /></p>
                <p><input type="text" value="{{ $task->getDescription() }}" placeholder="Description..." name="description" class="form-control" /></p>

                <hr>

                <p><button class="form-control btn btn-success">Edit Task</button></p>
            </form>
        </div>
    </div>
@endsection