@php /** @var $task \TodoList\Entities\Task */@endphp
@extends('layouts.master')
@section('title') Add a Task @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Tasks List</h3>
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Importance</th>
                        <th>Status</th>
                        <th>Operations</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr @if($task->isDone()) class="table-success" @endif>
                        <td>{{$loop->index}}</td>
                        <td>{{$task->getName()}}</td>
                        <td>{{$task->getDescription()}}</td>
                        <td>{{$task->getImportance()}}</td>
                        <td width="155">
                            @if($task->isDone())
                                Done
                            @else
                                Not done
                            @endif
                        </td>
                        <th>
                            <div class="d-flex">
                                <a href="{{ route('tasks.edit', ['id' => $task->getId()])}}" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{route('tasks.toggle',['id' => $task->getId()])}}" method="POST" class="pl-3">
                                    <input type="hidden" name="_method" value="PUT">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                </form>
                                <form action="{{route('tasks.destroy', ['id' => $task->getId()])}}" method="POST" class="pl-3">
                                    <input type="hidden" name="_method" value="DELETE">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </form>
                            </div>
                        </th>
                    </tr>
                @empty
                </tbody>
                <tr>
                    <td colspan="5">No task in the list...</td>
                </tr>
                @endforelse
            </table>
        </div>
    </div>
@endsection