@extends('layouts.master')
@section('title') Add a Task @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('tasks.index') }}" class="btn btn-sm btn-primary d-inline-block mb-3">
                <i class="fa fa-arrow-left"></i>
            </a>
            <h3>Add Task</h3>
            <p>Use the following form to <u>add</u> task.</p>

            <hr>

            <form action="{{ url('add') }}" method="post">
                {{ csrf_field() }}

                <p><input autofocus type="text" placeholder="Name..." name="name" class="form-control" /></p>
                <p><input type="text" placeholder="Description..." name="description" class="form-control" /></p>

                <hr>

                <p><button class="form-control btn btn-success">Add Task</button></p>
            </form>
        </div>
    </div>
@endsection